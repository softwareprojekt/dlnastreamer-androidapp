/* Author: Uenal Akkaya
 * Class for handling touch events.
 * touch events: click (short touch), long click (long touch), scroll (touch move) and zoom (multi touch gesture)
 */

package com.example.streamingapp;

import java.util.Timer;
import java.util.TimerTask;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class OnTouchHandler {
	private final static float SCROLL_THRESHOLD = 20;	//px
	private final static float ZOOM_THRESHOLD = 100;	//px
	private final static int LONG_CLICK_TIMEOUT = 1500;	//ms
	
	private static ContentController cc = ContentController.getInstance();
	// coordinates of click
	private static float mDownX = 0;
	private static float mDownY = 0;
	// distance of two click points
	private static float mDistance;
	
	private static boolean isOnClick = false;
	private static boolean isLongClick = false;
	private static boolean isOnScrolling = false;
	private static boolean isZooming = false;
	private static Timer longClickTimer;
	
	/**
	 * Contructor
	 */
	public OnTouchHandler() {}
	
	/**
	 * handle the touch events
	 * @param v	View
	 * @param e	MotionEvent
	 * @return	false
	 */
	public boolean handleOnTouchEvent(final View v, MotionEvent e) {
		//logInfo("actual: "+e.getX()+"/"+e.getY()+" mDown: "+mDownX+"/"+mDownY);
		
		switch(e.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:	// first touch on screen
				mDownX = e.getX();
				mDownY = e.getY();
				
				isLongClick = false;
				isOnScrolling = false;
				isZooming = false;
				
				if(mDownX < 20) {	// tolerance, don't get in conflict with action for navigation drawer!
					isOnClick = false;
				}
				else {
					isOnClick = true;
					
					if(longClickTimer != null) longClickTimer.cancel();
					
					longClickTimer = new Timer();
					longClickTimer.schedule(new TimerTask() {
						@Override
						public void run() {
								isOnClick = false;
								longClick(v);
						}
					}, LONG_CLICK_TIMEOUT);
				}
				break;
			case MotionEvent.ACTION_POINTER_DOWN:	// another touch on screen
				isOnClick = false;
				isLongClick = false;
				isOnScrolling = false;
				if(longClickTimer != null) longClickTimer.cancel();
				isZooming = true;
				
				mDistance = (float) Math.sqrt(Math.pow((e.getX(0)-e.getX(1)),2)+Math.pow((e.getY(0)-e.getY(1)),2));
				
				break;
			case MotionEvent.ACTION_POINTER_UP:		// touch released
				isZooming = false;
				
				logInfo("ACTION_POINTER_UP");
				
				break;
			case MotionEvent.ACTION_CANCEL:			// touch released or cancelled
			case MotionEvent.ACTION_UP:
				if(isOnClick && !isLongClick) {
					click(v);
				}
				if(longClickTimer != null) longClickTimer.cancel();
				break;
			case MotionEvent.ACTION_MOVE:			// touch coordinates changed
				if(isZooming) {
					float newDistance = (float) Math.sqrt(Math.pow((e.getX(0)-e.getX(1)),2)+Math.pow((e.getY(0)-e.getY(1)),2));
					float diff = newDistance-mDistance;
					
					if(diff > ZOOM_THRESHOLD) {
						logInfo("zoom in");
						zoom(10);
						mDistance = newDistance;
					}
					else if(diff < -ZOOM_THRESHOLD) {
						logInfo("zoom out");
						zoom(-10);
						mDistance = newDistance;
					}
				}
				
				int dx = (int) (mDownX - e.getX());
				int dy = (int) (mDownY - e.getY());
								
				if((isOnClick || isOnScrolling) && !isZooming && (Math.abs(dx) >= SCROLL_THRESHOLD || Math.abs(dy) >= SCROLL_THRESHOLD)) {
					isOnClick = false;
					isLongClick = false;
					isOnScrolling = true;
					mDownX = e.getX(); mDownY = e.getY();
					
					scroll(dx, dy);
				}
				break;
			default:
				logError("nothing matched! Action: "+e.getAction());
				isOnClick = false;
				isLongClick = false;
				if(longClickTimer != null) longClickTimer.cancel();
		}
		
		return true;
	}
	
	/**
	 * perform a scrolling on the web site
	 * @param dx	scroll in x direction
	 * @param dy	scroll in y direction
	 * @return		true if successful
	 */
	private boolean scroll(int dx, int dy) {
		logInfo("scroll: "+dx+"/"+dy);
		return cc.scroll(dx, dy);
	}

	/**
	 * perform a click on the web site
	 * @param v	View
	 * @return	true if successful
	 */
	private boolean click(View view) {
		int[] v = {(int) mDownX, (int) mDownY};
		normalizeCoords(view, v);
		logInfo("click: "+v[0]+"/"+v[1]);
		return cc.click(v[0], v[1], ContentController.CLICK.LEFT);
	}
	
	/**
	 * perform a right click on the web site
	 * @param v	View
	 * @return	true if successful
	 */
	private boolean longClick(View view) {
		int[] v = {(int) mDownX, (int) mDownY};
		normalizeCoords(view, v);
		logInfo("longClick: "+v[0]+"/"+v[1]);
		return cc.click(v[0], v[1], ContentController.CLICK.RIGHT);
	}
	
	/**
	 * zoom in/out the web page
	 * @param factor	zoom factor
	 * @return			true if successful
	 */
	private boolean zoom(int factor) {
		return cc.zoom(factor);
	}
	
	/**
	 * normalize the coordinates to a value between 0 and 1000
	 * @param v	vector with coordinates x and y
	 */
	private void normalizeCoords(View vView, int[] v) {
		int viewWidth = vView.getWidth(), viewHeight = vView.getHeight();
		
		logInfo("normalize(view): "+viewWidth+"/"+viewHeight);
		logInfo("normalize(coords): "+v[0]+"/"+v[1]);
		
		int newX = (v[0]*1000)/viewWidth;
		int newY = (v[1]*1000)/viewHeight;
		
		v[0] = newX; v[1] = newY;
	}
	
	/**
	 * log the info
	 * @param msg
	 */
	private void logInfo(String msg) {
		Log.i("OnTouchHandler", msg);
	}
	
	/**
	 * log the error
	 * @param msg
	 */
	private void logError(String msg) {
		Log.e("OnTouchHandler", msg);
	}
}