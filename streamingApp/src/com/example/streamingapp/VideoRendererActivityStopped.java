/* Author: Uenal Akkaya
 * Class which represents the state that media has stopped
 */

package com.example.streamingapp;

import java.net.URI;

import org.teleal.cling.support.avtransport.impl.state.AbstractState;
import org.teleal.cling.support.avtransport.impl.state.Stopped;
import org.teleal.cling.support.model.AVTransport;
import org.teleal.cling.support.model.SeekMode;

public class VideoRendererActivityStopped extends Stopped {

	public VideoRendererActivityStopped(AVTransport transport) {
		super(transport);
	}
	
	@Override
	public void onEntry() {
		super.onEntry();
	}
	
	public void onExit() {
	}

	@Override
	public Class<? extends AbstractState> next() {
		return VideoRendererActivityStopped.class;
	}

	@Override
	public Class<? extends AbstractState> play(String arg0) {
		return VideoRendererActivityPlaying.class;
	}

	@Override
	public Class<? extends AbstractState> previous() {
		return VideoRendererActivityStopped.class;
	}

	@Override
	public Class<? extends AbstractState> seek(SeekMode arg0, String arg1) {
		return VideoRendererActivityStopped.class;
	}

	@Override
	public Class<? extends AbstractState> setTransportURI(URI uri, String s) {
		return VideoRendererActivityStopped.class;
	}

	@Override
	public Class<? extends AbstractState> stop() {
		return VideoRendererActivityStopped.class;
	}
	
}