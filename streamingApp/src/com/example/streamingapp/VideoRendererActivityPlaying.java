/* Author: Uenal Akkaya
 * Class which represents the state that media is playing
 */

package com.example.streamingapp;

import java.net.URI;

import org.teleal.cling.support.avtransport.impl.state.AbstractState;
import org.teleal.cling.support.avtransport.impl.state.Playing;
import org.teleal.cling.support.model.AVTransport;
import org.teleal.cling.support.model.SeekMode;

public class VideoRendererActivityPlaying  extends Playing {

	public VideoRendererActivityPlaying(AVTransport transport) {
		super(transport);
	}

	@Override
    public void onEntry() {
        super.onEntry();
        // Start playing now!
    }

    @Override
    public Class<? extends AbstractState> setTransportURI(URI uri, String metaData) {
        return VideoRendererActivityStopped.class;
    }

    @Override
    public Class<? extends AbstractState> stop() {
        return VideoRendererActivityStopped.class;
    }

	@Override
	public Class next() {
		return null;
	}

	@Override
	public Class pause() {
		return null;
	}

	@Override
	public Class play(String arg0) {
		return null;
	}

	@Override
	public Class previous() {
		return null;
	}

	@Override
	public Class seek(SeekMode arg0, String arg1) {
		return null;
	}
}