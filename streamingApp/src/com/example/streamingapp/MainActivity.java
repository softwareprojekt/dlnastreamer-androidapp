/* Author: Uenal Akkaya
 * MainActivity which starts the App.
 */

package com.example.streamingapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// start the UpnpBrowser Activity to select the server
		Intent intent = new Intent();
		intent.setClassName(getPackageName(), getPackageName()+".UpnpBrowser");
		intent.putExtra("toTV", false);
		MainActivity.this.startActivity(intent);
		
		finish();
	}
}