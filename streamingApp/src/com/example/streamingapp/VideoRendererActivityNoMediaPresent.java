/* Author: Uenal Akkaya
 * Class which represents the state that no media is present
 */

package com.example.streamingapp;

import java.net.URI;

import org.teleal.cling.support.avtransport.impl.state.AbstractState;
import org.teleal.cling.support.avtransport.impl.state.NoMediaPresent;
import org.teleal.cling.support.avtransport.lastchange.AVTransportVariable;
import org.teleal.cling.support.model.AVTransport;
import org.teleal.cling.support.model.MediaInfo;
import org.teleal.cling.support.model.PositionInfo;


public class VideoRendererActivityNoMediaPresent extends NoMediaPresent {
	public VideoRendererActivityNoMediaPresent(AVTransport transport) {
		super(transport);
	}

	@Override
	public Class<? extends AbstractState> setTransportURI(URI uri, String s) {
		getTransport().setMediaInfo(new MediaInfo(uri.toString(), s));
		getTransport().setPositionInfo(new PositionInfo(1, s, uri.toString()));
		getTransport().getLastChange().setEventedValue(getTransport().getInstanceId(), new AVTransportVariable.AVTransportURI(uri), new AVTransportVariable.CurrentTrackURI(uri));
		return VideoRendererActivityStopped.class;
	}
}