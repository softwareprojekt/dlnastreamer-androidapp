/* Author: Uenal Akkaya
 * Activity which shows the stream of the server and handles touch events.
 */

package com.example.streamingapp;

import org.teleal.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.teleal.cling.model.meta.LocalService;
import org.teleal.cling.support.avtransport.impl.AVTransportService;
import org.teleal.cling.support.avtransport.impl.AVTransportStateMachine;
import org.teleal.common.statemachine.States;

import com.example.streamingapp.ContentController.RENDERER_CONTROLL_KEY;
import com.example.streamingapp.ContentController.RENDERER_CONTROLL_VALUE;
import com.example.streamingapp.ContentController.VIDEO_CONTROLL;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoRendererActivity extends Activity  {
	private int DIALOG_SHOW_DURATION = 1000;
	private String URL_EXTENSION_VIDEO = ":8544/screen";
	
	private VideoView vView = null;
	private NavigationDrawerListView adapter;
	private DrawerLayout mDrawerLayout;
    private ListView mListView;
	
	private ContentController cc = null;
	private Boolean videoPlaying = true;
	private Boolean videoPausedButtonIsShowing = true;
	private String[] videoElementsPlaying;
	private String[] videoElementsPaused;
	 
    private Integer[] videoElementsImagePlaying = { R.drawable.play_2, R.drawable.stop_1, R.drawable.totv_1, R.drawable.url_1, R.drawable.text, R.drawable.refresh_1, R.drawable.backward_1, R.drawable.forward_1, R.drawable.fullscreen_1};
	private Integer[] videoElementsImagePaused = { R.drawable.pause_1, R.drawable.stop_1, R.drawable.totv_1, R.drawable.url_1, R.drawable.text, R.drawable.refresh_1, R.drawable.backward_1, R.drawable.forward_1, R.drawable.fullscreen_1};
	
	// definitions of states for player
	@States({
		VideoRendererActivityNoMediaPresent.class,
		VideoRendererActivityStopped.class
	}) interface VideoRendererActivityStateMachine extends AVTransportStateMachine {}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_render_layout_nd);
		getActionBar().setTitle("Video Stream");
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_Layout);
        mListView = (ListView) findViewById(R.id.left_drawer);
       
        // Texts for ListView are saved as StringArray
        videoElementsPlaying = getResources().getStringArray(R.array.video_elements_playing);	
        videoElementsPaused = getResources().getStringArray(R.array.video_elements_paused);
        
        setAdapter();
        mListView.setBackgroundResource(R.color.LightBlue);
        mListView.setSelector(android.R.color.holo_blue_dark);       
        mListView.setOnItemClickListener(new OnItemClickListener() {
        
        	@Override
	        public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
	        	Log.i("Navigation Drawer", Integer.toString(position));
	        	
	        	setAdapter();
	        	mDrawerLayout.closeDrawers();
	        	
	        	switch(position) {
	        		case 0:
	        			playStreaming();
	        			videoPausedButtonIsShowing = !videoPausedButtonIsShowing;
	        			setAdapter();
	        			break;
	        		case 1: stopStreaming();
	        			videoPausedButtonIsShowing = false;
	        			setAdapter();
	        			break;
	        		case 2: streamToTV(); break;
	        		case 3: dialogNewURL(); break;
	        		case 4: dialogNewText(); break;
	        		case 5: refreshPage(); break;
	        		case 6: pageBack(); break;
	        		case 7: pageForward(); break;
	        		case 8: fullscreen(); break;
	        		default: showDialog("error on item selecting");
	        	}
	        }
        });
                
		cc = ContentController.getInstance();
		cc.setContext(getApplicationContext());
		String videoURL = cc.getServerURL()+URL_EXTENSION_VIDEO;
		videoURL = videoURL.replace("http", "rtsp");
		
		vView = (VideoView) findViewById(R.id.video_renderer_videoView);
		MediaController mc = new MediaController(this);
        vView.setMediaController(mc);
		Log.i("VRA", "videoURL: "+videoURL);
		
		final OnTouchHandler touchHandler = new OnTouchHandler();
		vView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return touchHandler.handleOnTouchEvent(v, event);
			}
		});
		
		Uri uri = Uri.parse(videoURL);
		vView.setVideoURI(uri);
		vView.requestFocus();
		vView.start();
	}
	
	/**
	 * (resume with playing of the stream to Tablet)
	 */
	@Override
	public void onResume() {
		super.onResume();
		Log.i("VRA","onResume()");
		playStreaming();
	}
	
	/**
	 * send play/pause command to server
	 */
	private void playStreaming() {
		if(!videoPlaying) {
			cc.controllVideo(ContentController.VIDEO_CONTROLL.PLAY);
			videoPlaying = true;
		}
		else {
			cc.controllVideo(ContentController.VIDEO_CONTROLL.PAUSE);
			videoPlaying = false;
		}
	}
	
	/**
	 * send stop command to server
	 */
	private void stopStreaming() {
		cc.controllVideo(ContentController.VIDEO_CONTROLL.STOP);
		videoPlaying = false;
	}
	
	/**
	 * start new Activity to select the TV for streaming
	 */
	private void streamToTV() {
		stopStreaming();
		Intent intent = new Intent();
		intent.setClassName(getPackageName(), getPackageName()+".SelectTV");
		intent.putExtra("toTV", true);
		Log.i("VRA","starting new Activity (SelectTV)");
		VideoRendererActivity.this.startActivity(intent);
	}
	
	private void fullscreen() {
		cc.controllVideo(VIDEO_CONTROLL.FULLSCREEN);
	}
	
	/**
	 * Hardware button listener.
	 * Get hardware buttons for changing volume of stream/video
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.i("KeyCode ", Integer.toString(keyCode));
		
		if((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
        	cc.controllRenderer(ContentController.RENDERER_CONTROLL_KEY.VLOUME, ContentController.RENDERER_CONTROLL_VALUE.VOLUME_DOWN);
        	Log.i("Volume","Down");
        	
        	 return true;
        }
        else if((keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
        	cc.controllRenderer(ContentController.RENDERER_CONTROLL_KEY.VLOUME, ContentController.RENDERER_CONTROLL_VALUE.VOLUME_UP);
        	Log.i("Volume","UP");
         	
        	return true;
        }
        else if((keyCode == KeyEvent.KEYCODE_BACK)) {
        	finish();
        	return true;
        }
            
        return false;
    }
	
	/**
	 * set the adapter for the navigation drawer
	 */
	public void setAdapter(){
		 if(!this.videoPausedButtonIsShowing) {
	        	this.adapter =  new NavigationDrawerListView(VideoRendererActivity.this, videoElementsPlaying, videoElementsImagePlaying);
		 }
	     else {
	       	this.adapter = new NavigationDrawerListView(VideoRendererActivity.this, videoElementsPaused, videoElementsImagePaused);
	     }         
	     this.mListView.setAdapter(this.adapter);
	}
	
	/**
	 * shows a dialog with an input for the new URL for the web site
	 */
	private void dialogNewURL() {
		final EditText input = new EditText(this);

		new AlertDialog.Builder(this).setTitle("New URL").setMessage("Enter new URL:").setView(input)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Editable val = input.getText();
					setNewURL(val.toString());
				}
			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {}
			}).show();
	}
	
	/**
	 * sets the new URL for the web site
	 * @param s	the new URL
	 */
	private void setNewURL(String s) {
		if(s == null) return;
		s.toLowerCase();
		if(!s.matches("^https?://.*")) s = "http://"+s;
		
		showDialog("New URL: "+s);
		cc.sendURL(s, null);
	}
	
	/**
	 * shows a dialog with an input for the new text to set
	 */
	private void dialogNewText() {
		final EditText input = new EditText(this);

		new AlertDialog.Builder(this).setTitle("New Text").setMessage("Enter new Text:").setView(input)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Editable val = input.getText();
					setNewText(val.toString());
				}
			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {}
			}).show();
	}
	
	/**
	 * sets the new text for the web site
	 * @param text
	 */
	private void setNewText(String text) {
		if(text == null) return;
		showDialog("New Text: "+text);
		
		cc.sendText(text);
	}
	
	/**
	 * send refreshPage command to server
	 */
	private void refreshPage() {
		cc.pageRefresh();
	}
	
	/**
	 * send pageBack command to server
	 */
	private void pageBack(){
		cc.pageBack();
	}
	
	/**
	 * send pageForward command to server
	 */
	private void pageForward() {
		cc.pageForward();
	}
	
	/**
	 * shows a info dialog of a string
	 * @param s	the info text
	 */
	private void showDialog(String s) {
		Toast.makeText(getApplicationContext(), s, DIALOG_SHOW_DURATION).show();
	}
}