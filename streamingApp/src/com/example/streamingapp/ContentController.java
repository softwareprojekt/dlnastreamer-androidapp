/* Author: Uenal Akkaya
 * Class for handling the connection and communication with the server.
 */

package com.example.streamingapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

public class ContentController {
	public static enum CLICK {LEFT, RIGHT};
	public static enum STATE {CLICKED};
	public static enum VIDEO_CONTROLL {PLAY, PAUSE, STOP, FULLSCREEN};
	public static enum RENDERER_CONTROLL_KEY {VLOUME, KEY};
	public static enum RENDERER_CONTROLL_VALUE {VOLUME_UP, VOLUME_DOWN, PLAY, PAUSE, STOP, POWEROFF};
	
	private final int HTTP_REQUEST_TIMEOUT = 700;
	private final int PORT = 10980;
	
	private static String baseURL = null;	// := http://[ip]
	private static String response = null;	// response of http request
	private static Context context = null;
	private HttpClient httpClient = null;
	
	private static ContentController cc = new ContentController();
	
	/**
	 * Constructor
	 */
	private ContentController() {
		HttpParams httpParams = new BasicHttpParams();
		this.httpClient = new DefaultHttpClient(httpParams);
		HttpConnectionParams.setConnectionTimeout(httpParams, HTTP_REQUEST_TIMEOUT);
	}
	
	/**
	 * @return	Instance of ContentController
	 */
	public static ContentController getInstance() {
		return cc;
	}
	
	/**
	 * set the Context
	 * @param c	Context c to set
	 */
	public void setContext(Context c) {
		context = c;
	}
	
	public void setServerURL(String url) {
		if(!url.matches("^https?://.*"))
			baseURL = "http://"+url;
		else
			baseURL = url;
	}
	
	/**
	 * @return	the URL of the server
	 */
	public String getServerURL() {
		return baseURL;
	}
	
	/**
	 * Send a message to the server in form of http://[serverip]:[port]/[method]?[param1=value1&param2=value2&...]
	 * @param msg	the message in form of [method]?[param1=value1&param2=value2&...]
	 * @return		true if request was successful
	 */
	private boolean sendHTTPRequest(String msg) {
		String newURL = baseURL+":"+PORT+"/"+msg;
		response = null;
		
		new SendHTTPRequestAsync().execute(newURL);
		return true;
	}
	
	/**
	 * AsyncTask for network connections
	 * params[0] is the URL
	 * result is the response
	 */
	private class SendHTTPRequestAsync extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			if(params.length == 0) return "";
			String newURL = params[0];
			HttpResponse httpResponse;
			response = null;
			
			logInfo("newURL: "+newURL);
			
			try {
				httpResponse = httpClient.execute(new HttpGet(newURL));
				logInfo(httpResponse.getStatusLine().toString());
				HttpEntity entity = httpResponse.getEntity();
				
				if(entity != null) {
					InputStream inStream = entity.getContent();
					response = convertStreamToString(inStream);
					inStream.close();
				}
				
				return response;
			} catch(Exception e) {
				logError(e.toString());
				return "error";
			}
		}
		
		@Override
		protected void onPostExecute(String result) {
			logInfo("result: "+result);
		}
	}
	
	/**
	 * @return	response of server
	 */
	public String getResponse() {
		return response==null ? "" : response;
	}
	
	/**
	 * Convert a input stream to a string
	 * source: http://pastebin.com/Uzzwpz5g
	 * 
	 * @param is			the input stream
	 * @return				stringified input stream
	 * @throws IOException
	 */
	private String convertStreamToString(InputStream is) throws IOException {
		if (is != null) {
			StringBuilder sb = new StringBuilder();
			String line;
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "UTF-8"));
				while ((line = reader.readLine()) != null) {
					sb.append(line).append("\n");
				}
			} 
			finally {
				is.close();
			}
			return sb.toString();
		} else {
			return "";
		}
	}
	
	/**
	 * Send a click command to the server
	 * @param x	the x coordinate
	 * @param y	the y coordinate
	 * @param c	the mouse button which should be clicked
	 * @return	true if successful
	 */
	public boolean click(int x, int y, CLICK c) {
		String newMsg = "click?x="+x+"&y="+y+"&key="+c;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send a mouse move command to the server
	 * @param x	the x coordinate
	 * @param y	the y coordinate
	 * @param s	the state
	 * @return	true if successful
	 */
	public boolean move(int x, int y, STATE s) {
		String newMsg = "move?x="+x+"&y="+y+"&state="+s;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send a scroll command to the server
	 * @param dx	value of horizontal scroll
	 * @param dy	value of vertical scroll
	 * @return		true if successful
	 */
	public boolean scroll(int dx, int dy) {
		String newMsg = "scroll?dx="+dx+"&dy="+dy;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send a zoom command to the server
	 * @param factor	the zoom factor
	 * @return			true if successful
	 */
	public boolean zoom(int factor) {
		String newMsg = "zoom?factor="+factor;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * get the list of available renderer from server
	 * @return	true if successful
	 */
	public boolean getRenderer() {
		String newMsg = "getRenderer";
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Set the renderer to a specific ip [device] number
	 * @return	true if successful
	 */
	public boolean setRenderer(String ip) {
		String newMsg = "setRenderer?ip="+ip;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send a command to the server to control the video status
	 * 
	 * @param c	the control command to steer the video {PLAY, PAUSE, STOP}
	 * @return	true if successful
	 */
	public boolean controllVideo(VIDEO_CONTROLL c) {
		String newMsg = "controllVideo?key="+c;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send a renderer control command to the server
	 * 
	 * @param k		the key {VOLUME, KEY}
	 * @param rcv	the value {VOLUME_UP, VOLUME_DOWN, KEY}
	 * @return		true if successful
	 */
	public boolean controllRenderer(RENDERER_CONTROLL_KEY k, RENDERER_CONTROLL_VALUE rcv) {
		String newMsg = "controllRenderer?key="+k+"&value="+rcv;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Get the status from server
	 * @return	true if successful
	 */
	public boolean getStatus() {
		String newMsg = "getStatus";
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Sends a new url for the browser to the server
	 * @param url	the requested url
	 * @param c		cookie [not implemented yet]
	 * @return		true if successful
	 */
	public boolean sendURL(String url, Cookie c) {
		String newMsg = "sendURL?url="+url+"&cookie="+c;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send a input message to the server
	 * @param text	the message
	 * @return		true if successful
	 */
	public boolean sendText(String text) {
		String newMsg = "sendText?text="+text;
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send the command to go a page forward
	 * @return	true if successful
	 */
	public boolean pageForward() {
		String newMsg = "pageForward";
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send the command to go a page back
	 * @return	true if successful
	 */
	public boolean pageBack() {
		String newMsg = "pageBack";
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * Send the command to refresh the page
	 * @return	true if successful
	 */
	public boolean pageRefresh() {
		String newMsg = "refreshPage";
		return sendHTTPRequest(newMsg);
	}
	
	/**
	 * determine the own device ip address in the network
	 * @return	the own device ip address
	 */
	public String getIpAddress() {
		if(context == null) return null;
		
		WifiManager wManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wManager.getConnectionInfo();
		int ip = wInfo.getIpAddress();
		
		String ipString = String.format("%d.%d.%d.%d", (ip & 0xff), (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
		return ipString;
	}
	
	/**
	 * Log new message on info level
	 * @param msg
	 */
	private void logInfo(String msg) {
		Log.i("ContentController", msg);
	}
	
	/**
	 * Log new message on error level
	 * @param msg
	 */
	private void logError(String msg) {
		Log.e("ContentControllerError", msg);
	}
}