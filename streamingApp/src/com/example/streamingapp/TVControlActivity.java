/* Author: Uenal Akkaya
 * Activity for controlling the streaming on TV.
 */

package com.example.streamingapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

public class TVControlActivity extends Activity {
	// Buttons
	private Button playButton = null;
	private Button stopButton = null;
	private Button noTVButton = null;
	
	private boolean playing = true;
	private ContentController cc = ContentController.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.example.streamingapp.R.layout.tv_control_elements_layout);
		getActionBar().setTitle("TV Controller");
		
		setButtons();
	}
	
	/**
	 * Hardware button listener.
	 * Get hardware buttons for changing volume of stream/video
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
        	cc.controllRenderer(ContentController.RENDERER_CONTROLL_KEY.VLOUME, ContentController.RENDERER_CONTROLL_VALUE.VOLUME_DOWN);
        	Log.i("Volume","Down");
        	
        	return true;
        } else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
        	cc.controllRenderer(ContentController.RENDERER_CONTROLL_KEY.VLOUME, ContentController.RENDERER_CONTROLL_VALUE.VOLUME_UP);
        	Log.i("Volume","UP");
        	
        	return true;
        } else if((keyCode == KeyEvent.KEYCODE_BACK)){
        	finish();
        	return true;
        }
        
        return true;
    }
	
	/**
	 * set the buttons
	 */
	private void setButtons() {
		playButton = (Button) findViewById(com.example.streamingapp.R.id.tv_control_Button_play);
		stopButton = (Button) findViewById(com.example.streamingapp.R.id.tv_control_Button_stop);
		noTVButton = (Button) findViewById(com.example.streamingapp.R.id.tv_control_Button_noTV);
		playButton.setText(com.example.streamingapp.R.string.tv_control_Pause);
		
		setButtonsOnClickListeners();
	}
	
	/**
	 * Sets all OnClickListeners of buttons
	 */
	private void setButtonsOnClickListeners() {
		playButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(playing) {
					cc.controllVideo(ContentController.VIDEO_CONTROLL.PAUSE);
					playButton.setText(com.example.streamingapp.R.string.tv_control_Play);
					playing = false;
				}
				else {
					cc.controllVideo(ContentController.VIDEO_CONTROLL.PLAY);
					playButton.setText(com.example.streamingapp.R.string.tv_control_Pause);
					playing = true;
				}
			}
		});
		
		stopButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cc.controllVideo(ContentController.VIDEO_CONTROLL.STOP);
				playButton.setText(com.example.streamingapp.R.string.tv_control_Play);
				playing = false;
			}
		});
		
		noTVButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cc.setRenderer(cc.getIpAddress());
				finish();
			}
		});
	}
}