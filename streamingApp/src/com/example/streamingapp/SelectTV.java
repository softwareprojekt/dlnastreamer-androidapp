/* Author: Uenal Akkaya
 * Activity which fetch the list of available TVs from server and show them in a list.
 */

package com.example.streamingapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.streamingapp.ContentController.RENDERER_CONTROLL_KEY;
import com.example.streamingapp.ContentController.RENDERER_CONTROLL_VALUE;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class SelectTV extends ListActivity {
	private ContentController cc = ContentController.getInstance();
	private ArrayAdapter<String> listAdapter = null;
	private ProgressDialog progress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		setListAdapter(listAdapter);
		getActionBar().setTitle("Select TV");
				
		getListView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(position==0) {			// at position 0 is the refresh button
					showProgressDialog();
					listAdapter.clear();
					new RefreshTVList().execute();
					return;
				}
				
				String ip = getIPFromItem(listAdapter.getItem(position));
				cc.setRenderer(ip);
				cc.controllRenderer(RENDERER_CONTROLL_KEY.KEY, RENDERER_CONTROLL_VALUE.PLAY);
				Toast.makeText(getApplicationContext(), "new TVIP: "+ip, Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent();
				intent.setClassName(getPackageName(), getPackageName()+".TVControlActivity");
				SelectTV.this.startActivity(intent);
				finish();
			}
		});
		
		showProgressDialog();
		new RefreshTVList().execute();
	}
	
	/**
	 * shows a progress notification (while fetching new list)
	 */
	private void showProgressDialog() {
		progress = ProgressDialog.show(this, "Loading TVs", "Loading available TVs", true);
	}
	
	/**
	 * asynchronous task for fetching new list of TVs from TV
	 */
	private class RefreshTVList extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			cc.getRenderer();
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Log.e("STV",e.getMessage());
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			progress.dismiss();
			refreshList();
		}
	}
	
	/**
	 * refresh the list of available TVs
	 * the informations are in response of ContentController
	 */
	private void refreshList() {
		listAdapter.clear();
		
		if(cc.getResponse().equals("a")) {	// no TVs found
			listAdapter.add("No TVs found! Click to refresh list");
			return;
		}
		String s = cc.getResponse();//"[\n\t{\"uuid\":\"ich bin TV010\", \"ip\":\"192.168.0.1\", \"name\":\"cool TV\"},\n\t{\"uuid\":\"ich bin TV202\", \"ip\":\"192.168.0.5\", \"name\":\"much cooler TV\"}\n]";
		
		listAdapter.add("Refresh TV list");	
		//listAdapter.add(s);
		Log.d("STV onPostEx",s);
		
		try {
			addTVsFromJSONObjects(s);
		} catch (JSONException e) {
			Log.e("STV",e.getMessage());
		}
	}
	
	/**
	 * add all TVs (extracted from JSON-Array) to list of available TVs
	 * @param objects	JSONArray
	 * @throws JSONException
	 */
	private void addTVsFromJSONObjects(String objects) throws JSONException {
		JSONArray jsonArray = new JSONArray(objects);
		JSONObject jsonObject = new JSONObject();
				
		for(int i=0; i<jsonArray.length(); i++) {
			jsonObject = jsonArray.getJSONObject(i);
			String uuid = jsonObject.getString("uuid");
			String name = jsonObject.getString("name");
			String ip = jsonObject.getString("ip");
			Log.i("STV", i+": "+uuid+" "+name+" "+ip);
			
			listAdapter.add("UUID:   "+uuid+"\nName: "+name+"\nIP:        "+ip);
		}
	}
	
	/**
	 * extract the IP from String which is in form "[name]\n[ip]"
	 * @param item	String with name+IP
	 * @return		String with IP
	 */
	private String getIPFromItem(String item) {
		String s = item.substring(item.lastIndexOf(" ")+1);
		return s;
	}
}