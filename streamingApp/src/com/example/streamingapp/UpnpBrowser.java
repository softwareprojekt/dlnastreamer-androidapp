/* Author: Uenal Akkaya
 * Activity which fetch the list of available UPNP devices and show them in a list.
 * (modified version of "Cling for Android" from http://4thline.org/projects/cling/core/manual/cling-core-manual.html)
 */

package com.example.streamingapp;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.android.AndroidUpnpServiceConfiguration;
import org.teleal.cling.android.AndroidUpnpServiceImpl;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UDAServiceType;
import org.teleal.cling.protocol.async.SendingNotificationAlive;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.registry.RegistryListener;

import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.streamingapp.ContentController.RENDERER_CONTROLL_KEY;
import com.example.streamingapp.ContentController.RENDERER_CONTROLL_VALUE;

public class UpnpBrowser extends ListActivity {
	private ContentController cc = ContentController.getInstance();
	private ArrayAdapter<DeviceDisplay> listAdapter;
	private AndroidUpnpService upnpService;
	private ServiceConnection serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
	        upnpService = (AndroidUpnpService) service;

	        // Refresh the list with all known devices
	        listAdapter.clear();
	        for (Device device : upnpService.getRegistry().getDevices()) {
	            ((BrowseRegistryListener) registryListener).deviceAdded(device);
	        }

	        // Getting ready for future device advertisements
	        upnpService.getRegistry().addListener(registryListener);

	        // Search asynchronously for all devices
	        upnpService.getControlPoint().search();
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	        upnpService = null;
	    }
	};
	private RegistryListener registryListener = new BrowseRegistryListener();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setTitle("Select Server");
		listAdapter = new ArrayAdapter<DeviceDisplay>(this, android.R.layout.simple_list_item_1);
		setListAdapter(listAdapter);
		
		getApplicationContext().bindService(new Intent(this, AndroidUpnpServiceImpl.class), serviceConnection, Context.BIND_AUTO_CREATE);
		
		getListView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				RemoteDevice d = ((RemoteDevice) listAdapter.getItem(position).getDevice());
				//String text = d.getIdentity().getDescriptorURL().toString();
				//String baseURL = getBaseURL(text);
				//Toast.makeText(getApplicationContext(), "clicked item: "+text+"\nbaseURL: "+baseURL, Toast.LENGTH_LONG).show();
				
				Intent intent = new Intent();
				cc.setServerURL(d.getIdentity().getDescriptorURL().getHost());
				Toast.makeText(getApplicationContext(), "new server URL: "+cc.getServerURL(), Toast.LENGTH_SHORT).show();
				
				SendingNotificationAlive sna;
				try {
					sna = new SendingNotificationAlive(upnpService.getRegistry().getUpnpService() , new LocalDevice(d.getIdentity()));
					sna.run();
					
					Log.i("UpnpB", "sna: SendingNotificationAlive: "+d.getDisplayString());
				} catch (Exception e) {
					Log.e("UpnpB", "sna: "+e.getMessage());
				}
				
				intent.setClassName(getPackageName(), getPackageName()+".VideoRendererActivity");
				UpnpBrowser.this.startActivity(intent);
			}

			private String getBaseURL(String s) {
				String regex = "^https?://\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}/"; 
				if(!s.matches(regex+".*")) return null;
				
				String k = s.replaceFirst(regex, "");
				return s.replace(k, "");
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(upnpService != null) {
			upnpService.getRegistry().removeListener(registryListener);
		}
		getApplicationContext().unbindService(serviceConnection);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    menu.add(0, 0, 0, R.string.search_lan)
	        .setIcon(android.R.drawable.ic_menu_search);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    if (item.getItemId() == 0 && upnpService != null) {
	        upnpService.getRegistry().removeAllRemoteDevices();
	        upnpService.getControlPoint().search();
	    }
	    return false;
	}
	
	class BrowseRegistryListener extends DefaultRegistryListener {
		@Override
	    public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice device) {
	        deviceAdded(device);
	    }
	
	    @Override
	    public void remoteDeviceDiscoveryFailed(Registry registry, final RemoteDevice device, final Exception ex) {
	        runOnUiThread(new Runnable() {
	            @Override
				public void run() {
	                Toast.makeText(
	                        UpnpBrowser.this,
	                        "Discovery failed of '" + device.getDisplayString() + "': " +
	                                (ex != null ? ex.toString() : "Couldn't retrieve device/service descriptors"),
	                        Toast.LENGTH_LONG
	                ).show();
	            }
	        });
	        deviceRemoved(device);
	    }
	
	    @Override
	    public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
	    	Log.i("UpnpBrowser", "remoteDeviceAdded: "+device.getDisplayString()+"\n "+device.getDetails().getBaseURL());
	    	Log.i("UpnpBrowser", "getHost: "+device.getIdentity().getDescriptorURL().getHost());
	    	Log.i("UpnpBrowser", "getDescriptorURL: "+device.getIdentity().getDescriptorURL());
	        deviceAdded(device);
	    }
	
	    @Override
	    public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
	    	Log.i("UpnpBrowser", "remoteDeviceRemoved: "+device.getDisplayString());
	        deviceRemoved(device);
	    }
	
	    @Override
	    public void localDeviceAdded(Registry registry, LocalDevice device) {
	    	Log.i("UpnpBrowser", "localDeviceAdded: "+device.getDisplayString());
	        deviceAdded(device);
	    }
	
	    @Override
	    public void localDeviceRemoved(Registry registry, LocalDevice device) {
	    	Log.i("UpnpBrowser", "localDeviceRemoved: "+device.getDisplayString());
	        deviceRemoved(device);
	    }
	
	    public void deviceAdded(final Device device) {
	        runOnUiThread(new Runnable() {
	            @Override
				public void run() {
	                DeviceDisplay d = new DeviceDisplay(device);
	                int position = listAdapter.getPosition(d);
	                if (position >= 0) {
	                    // Device already in the list, re-set new value at same position
	                    listAdapter.remove(d);
	                    listAdapter.insert(d, position);
	                } else {
	                    listAdapter.add(d);
	                }
	            }
	        });
	    }
	
	    public void deviceRemoved(final Device device) {
	        runOnUiThread(new Runnable() {
	            @Override
				public void run() {
	                listAdapter.remove(new DeviceDisplay(device));
	            }
	        });
	    }
	}
	
	class DeviceDisplay {
	    Device device;

	    public DeviceDisplay(Device device) {
	        this.device = device;
	    }

	    public Device getDevice() {
	        return device;
	    }

	    @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	        DeviceDisplay that = (DeviceDisplay) o;
	        return device.equals(that.device);
	    }

	    @Override
	    public int hashCode() {
	        return device.hashCode();
	    }

	    @Override
	    public String toString() {
	        // Display a little star while the device is being loaded
	        return device.isFullyHydrated() ? device.getDisplayString() : device.getDisplayString() + " *";
	    }
	}
	
	public class MyUpnpService extends AndroidUpnpServiceImpl {

	    @Override
	    protected AndroidUpnpServiceConfiguration createConfiguration(WifiManager wifiManager) {
	        return new AndroidUpnpServiceConfiguration(wifiManager) {

	            @Override
	            public int getRegistryMaintenanceIntervalMillis() {
	                return 7000;
	            }
	            
	            @Override
	            public ServiceType[] getExclusiveServiceTypes() {
	                return new ServiceType[] {
	                        new UDAServiceType("SwitchPower")
	                };
	            }
	        };
	    }
	}	
}