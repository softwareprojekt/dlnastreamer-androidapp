/* Author: Uenal Akkaya
 * Class for a customized ArrayAdapter for the Navigation Drawer with Icon + Text in ListView
 */

package com.example.streamingapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavigationDrawerListView extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] web;
	private final Integer[] imageId;
	
	public NavigationDrawerListView (Activity context, String[] web, Integer[] imageId) {
		super(context, R.layout.drawer_list_item, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {	
		LayoutInflater inflater = context.getLayoutInflater();
		
		View rowView= inflater.inflate(R.layout.drawer_list_item, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
		
		txtTitle.setText(web[position]);
		imageView.setImageResource(imageId[position]);
		return rowView;
	}
}